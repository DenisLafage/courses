\beamer@sectionintoc {1}{What is a GIS?}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Definition}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Softwares}{5}{0}{1}
\beamer@subsectionintoc {1}{3}{Geographical data}{6}{0}{1}
\beamer@sectionintoc {2}{Applications}{12}{0}{2}
\beamer@subsectionintoc {2}{1}{View data & create Maps}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Make spatial analyses}{17}{0}{2}
\beamer@sectionintoc {3}{Survival kit}{24}{0}{3}
\beamer@subsectionintoc {3}{1}{Geodesy}{25}{0}{3}
\beamer@subsectionintoc {3}{2}{Projections}{27}{0}{3}
\beamer@subsectionintoc {3}{3}{Creating feature}{33}{0}{3}
\beamer@subsectionintoc {3}{4}{Data management}{35}{0}{3}
\beamer@sectionintoc {4}{Practice}{44}{0}{4}
\select@language {english}
